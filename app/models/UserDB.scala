package models

import io.ebean.{Finder, Model}
import javax.persistence._
import scala.collection.JavaConverters._


@Entity
@Table(name = "userdb")
class UserDB extends Model {

  @Id
  var id: Long = 0
  var userName: String = ""
  var password: String = ""
  var email: String = ""
  @OneToMany(mappedBy = "userdb")
  var lucky: List[LuckyNumber] = _

  var find: Finder[Long, UserDB] = new Finder[Long, UserDB](classOf[UserDB])

  def all(): List[UserDB] = find.all.asScala.toList

  def findByName(name: String): List[UserDB] = {
    find.query().where.eq("user_name", name).findList().asScala.toList
  }
}
