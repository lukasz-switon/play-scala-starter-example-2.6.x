package models

import io.ebean.{Finder, Model}
import javax.persistence._

import scala.collection.JavaConverters._

@Entity
@Table(name = "luckyNumber")
class LuckyNumber extends Model {

  @Id
  var id: Long = 0
  var number: Int = 0
  @ManyToOne(cascade = Array(CascadeType.ALL))
  var user: UserDB = _

  var find: Finder[Long, LuckyNumber] = new Finder[Long, LuckyNumber](classOf[LuckyNumber])

  def all(): List[LuckyNumber] = find.all.asScala.toList

  def findByUserId(id: Long): List[LuckyNumber] = {
    find.query().where.eq("user_id", id).findList().asScala.toList
  }
}
