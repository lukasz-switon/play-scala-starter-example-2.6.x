package models

import anorm._
import java.sql.{Connection, DriverManager}

object Database {

  Class.forName("org.h2.Driver")
  implicit val connection: Connection = getConnection("jdbc:h2:mem:tests")

  def getConnection(url: String): Connection = {
    DriverManager.getConnection(url,"sa","")
  }

  def insert(): Unit = {
    SQL("insert into User(email, password) values ({email}, {password})")
      .on("email" -> "Test", "password" -> "Test").executeInsert()
  }

  def dropAndCreate() {
    drop()
    create()
    insert()
    val sql = SQL(
      """
        SELECT email FROM User
      """).as(SqlParser.scalar[String].single)
    println(sql)
  }

//  def select(): SqlQueryResult = {
//    SQL(
//      """
//        SELECT * FROM User
//      """)
//  }

  def drop() {
    SQL("""
        DROP TABLE IF EXISTS User;
      """).executeUpdate()
  }

  def create() {
    SQL("""
        CREATE TABLE User (
           id bigint(20) NOT NULL AUTO_INCREMENT,
           email varchar(255) NOT NULL,
           password varchar(255) NOT NULL,
           PRIMARY KEY (id)
        );
      """).executeUpdate()
  }
}