package controllers

import javax.inject._
import models.{LuckyNumber, UserDB, _}
import play.api.i18n.{I18nSupport, MessagesApi}
import play.api.mvc._
import org.mindrot.jbcrypt.BCrypt

@Singleton
class HomeController @Inject()(cc: ControllerComponents, messagesApi: MessagesApi)
  extends AbstractController(cc) with I18nSupport {

  def index: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Database.dropAndCreate()
    val user: UserDB = new UserDB
    val userList: List[UserDB] = user.all()
    Ok(views.html.index(request, user, userList))
  }

  def login: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.login(UserLogin.form))
  }

  def details(name: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val userList = new UserDB().findByName(name)
    val user = userList.head
    val luckyNumbers = new LuckyNumber().findByUserId(user.id)
    Ok(views.html.details(UserEmail.form, UserPassword.form, user, luckyNumbers))
  }

  def changeEmail(username: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    UserEmail.form.bindFromRequest.fold(
      _ => {
        Redirect("/")
      },
      formData => {
        val email = formData.email
        val userList = new UserDB().findByName(username)
        val user = userList.head
        user.email = email
        user.save()
        Redirect("/")
      }
    )
  }

  def changeLuckyNumbers(username: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    val userList = new UserDB().findByName(username)
    val user = userList.head
    val luckyNumbers = new LuckyNumber().findByUserId(user.id)
    val r = scala.util.Random
    for (luckyNumber <- luckyNumbers) luckyNumber.number = r.nextInt(100)
    for (luckyNumber <- luckyNumbers) luckyNumber.save()
    Redirect("/")
  }

  def changePassword(username: String): Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    UserPassword.form.bindFromRequest.fold(
      _ => {
        Redirect("/")
      },
      formData => {
        val password = formData.password
        val userList = new UserDB().findByName(username)
        val user = userList.head
        user.password = BCrypt.hashpw(password, BCrypt.gensalt)
        user.save()
        Redirect("/")
      }
    )
  }

  def logout: Action[AnyContent] = Action {
    Redirect("/").withSession("logged_user_session" -> "")
  }

  def register: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    Ok(views.html.register(User.form))
  }

  def submitRegister: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>

    User.form.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.register(formWithErrors))
      },
      formData => {
        if (userExist(formData)) {
          Redirect("/")
        } else {
          val hashedPassword = BCrypt.hashpw(formData.password, BCrypt.gensalt)
          val user = new UserDB
          user.userName = formData.username
          user.password = hashedPassword
          user.email = formData.email
          createLuckyNumber(user)
          user.save()
          Redirect("/").withSession("logged_user_session" -> formData.username)
        }

      }
    )
  }

  private def userExist(formData: User): Boolean = {
    val name = formData.username
    val users: List[UserDB] = new UserDB().findByName(name)
    users.nonEmpty
  }

  private def createLuckyNumber(user: UserDB): Unit = {
    val r = scala.util.Random
    var luckyNumber = new LuckyNumber
    luckyNumber.number = r.nextInt(100)
    luckyNumber.user = user
    luckyNumber.save()
    user.lucky = List(luckyNumber)
    luckyNumber = new LuckyNumber
    luckyNumber.number = r.nextInt(100)
    luckyNumber.user = user
    luckyNumber.save()
  }

  def submitLogin: Action[AnyContent] = Action { implicit request: Request[AnyContent] =>
    UserLogin.form.bindFromRequest.fold(
      formWithErrors => {
        BadRequest(views.html.login(formWithErrors))
      },
      formData => {
        val name = formData.username
        val password = formData.password
        val user = new UserDB
        val users: List[UserDB] = user.findByName(name)
        if (users.nonEmpty && BCrypt.checkpw(password.toString, users.head.password.toString)) {
          Redirect("/").withSession("logged_user_session" -> name)
        } else {
          Redirect("/")
        }
      }
    )
  }
}
